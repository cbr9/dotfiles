DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"
DIR=$(pwd)
sudo pacman-mirrors --api --set-branch unstable
sudo pacman-mirrors --fasttrack 5 && sudo pacman -Syyu
sudo pacman -S yay tree mlocate qemu virt-manager virt-viewer dnsmasq vde2 bridge-utils openbsd-netcat ebtables iptables
yay -S vim spotify plasma-workspace-agent-ssh libdbusmenu-glib appmenu-gtk-module jetbrains-toolbox webtorrent-desktop-bin qalculate-gtk oxygen-sounds plasma-browser-integration sierrabreeze-kwin-decoration-git zsh oh-my-zsh-git plasma5-applets-window-appmenu plasma5-applets-window-buttons plasma5-applets-window-title libreoffice-fresh bluez-utils bluez-hciconfig go latte-dock kvantum-qt5 mailspring gnome-keyring pcloud-drive go-ipfs libguestfs
sudo cp $DIR/.zshrc $HOME/
cp -r $DIR/.ssh $HOME/
sudo ln -s $DIR/ssh-add.sh ~/.config/autostart-scripts
sudo ln -s $DIR/ssh-agent.sh /lib/systemd/system-shutdown/ssh-agent.sh
sudo ln -s $DIR/ethernet.sh /lib/systemd/system-sleep
sudo cp $DIR/environment /etc/
sudo cp $DIR/kcmfonts $HOME/.config/kcmfonts
sudo chsh cabero -s /usr/bin/zsh
sudo systemctl enable libvirtd.service
sudo systemctl start libvirtd.service
reboot
